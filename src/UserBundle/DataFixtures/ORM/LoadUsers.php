<?php
/**
 * Created by PhpStorm.
 * User: ozzie_3000
 * Date: 28-06-16
 * Time: 23:50
 */

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\User;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setFirstname('Oscar');
        $user1->setLastname('Untied');
        $user1->setEmail('ozzie_3000@hotmail.com');
        $user1->setPlainPassword('wachtwoord');
        $user1->setAnotherfield('Iets over Oscar');
        $user1->setEnabled(true);
        $user1->setRoles(array('ROLE_ORG'));

        $manager->persist($user1);

        $user2 = new User();
        $user2->setFirstname('Sam  ');
        $user2->setLastname('Gosschalk');
        $user2->setEmail('samgosschalk@live.nl');
        $user2->setPlainPassword('wachtwoord');
        $user2->setAnotherfield('Iets over Sam');
        $user2->setEnabled(true);
        $user2->setRoles(array('ROLE_STD'));

        $manager->persist($user2);

        $user3 = new User();
        $user3->setFirstname('Rebekka');
        $user3->setLastname('Zamara');
        $user3->setEmail('rebzamara@gmail.com');
        $user3->setPlainPassword('wachtwoord');
        $user3->setAnotherfield('Iets over Rebekka');
        $user3->setEnabled(true);
        $user3->setRoles(array('ROLE_STD'));

        $manager->persist($user3);

        $user4 = new User();
        $user4->setFirstname('Thomas');
        $user4->setLastname('Glas');
        $user4->setEmail('thomasglas@gmail.com');
        $user4->setPlainPassword('wachtwoord');
        $user4->setAnotherfield('Iets over Thomas');
        $user4->setEnabled(true);
        $user4->setRoles(array('ROLE_UNI'));

        $manager->persist($user4);

        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
} 