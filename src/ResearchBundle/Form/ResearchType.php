<?php

namespace ResearchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('location')
            ->add('company')
            ->add('availabilityStarts', 'datetime', ['date_widget' => 'single_text', 'time_widget' => 'single_text','date_format' => 'dd-MM-yyyy'])
            ->add('availabilityEnds', 'datetime', ['date_widget' => 'single_text', 'time_widget' => 'single_text','date_format' => 'dd-MM-yyyy' ])
            ->add('startDate', 'date', ['widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'attr' => ['class' => 'form-control datepicker', 'type' => 'text', 'data-provide' => 'datepicker', 'data-date-format' => 'dd-MM-yyyy', 'placeholder' => 'dd-mm-jjjj']])
            ->add('endDate', 'date', ['widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'attr' => ['class' => 'form-control datepicker', 'type' => 'text', 'data-provide' => 'datepicker', 'data-date-format' => 'dd-MM-yyyy', 'placeholder' => 'dd-mm-jjjj']])
            ->add('inLanguage')
            ->add('organizer')
            ->add('description', 'textarea', ['attr' => ['class' => 'form-control', 'rows' => '2']])
            ->add('longDescription', 'textarea', ['attr' => ['class' => 'form-control', 'rows' => '5']])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ResearchBundle\Entity\Research'
        ));
    }
}
