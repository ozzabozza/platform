<?php
/**
 * Created by PhpStorm.
 * User: ozzie_3000
 * Date: 29-06-16
 * Time: 11:57
 */

namespace ResearchBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use ResearchBundle\Entity\Research;


class LoadResearches extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager)
    {

        $oscar= $manager->getRepository('UserBundle:User')
            ->findOneByUsername('ozzie_3000@hotmail.com');


        $research1 = new Research();
        $research1->setName('Population Growth in Miami');
        $research1->setLocation('Miami');
        $research1->setCompany('Sun Beach Population Institute');
        $research1->setAvailabilityStarts(new \DateTime('yesterday - 4days 10:00:00'));
        $research1->setAvailabilityEnds(new \DateTime('yesterday 22:00:00'));
        $research1->setStartDate(new \DateTime('01-08-2016'));
        $research1->setEndDate(new \DateTime('01-12-2016'));
        $research1->setInLanguage('English');
        $research1->setOrganizer('Green Planet Organization');
        $research1->setDescription('Miami gains popularity because of its sunny beaches, will the city stand its growth');
        $research1->setlongDescription('Miami this Miami that, lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus vel velit at dapibus. Mauris ut nulla sit amet erat molestie lacinia. Quisque nec sagittis urna. Vivamus ex elit, ullamcorper vitae tellus vel, egestas varius neque. Maecenas venenatis venenatis ipsum, sit amet vestibulum quam sollicitudin ac. Ut ornare, ligula ac elementum venenatis, nulla libero consequat risus, eu luctus tortor leo nec lorem. Curabitur fermentum sed neque sit amet elementum.');
        $research1->setOwner($oscar);
        $manager->persist($research1);

        $research2 = new Research();
        $research2->setName('Drinkwater Quality Dhaka');
        $research2->setLocation('Dhaka');
        $research2->setCompany('Dhaka Public Water Systems');
        $research2->setAvailabilityStarts(new \DateTime('tomorrow 10:00:00'));
        $research2->setAvailabilityEnds(new \DateTime('tomorrow + 1day 23:00:00'));
        $research2->setStartDate(new \DateTime('01-09-2016'));
        $research2->setEndDate(new \DateTime('01-01-2017'));
        $research2->setInLanguage('English');
        $research2->setOrganizer('Human Health Organization');
        $research2->setDescription('Is Dhakas drinking water causing health problems and what can be done');
        $research2->setlongDescription('Dhaka is big city with a very dens populations and almost no canalization, lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus vel velit at dapibus. Mauris ut nulla sit amet erat molestie lacinia. Quisque nec sagittis urna. Vivamus ex elit, ullamcorper vitae tellus vel, egestas varius neque. Maecenas venenatis venenatis ipsum, sit amet vestibulum quam sollicitudin ac. Ut ornare, ligula ac elementum venenatis, nulla libero consequat risus, eu luctus tortor leo nec lorem. Curabitur fermentum sed neque sit amet elementum.');
        $research2->setOwner($oscar);
        $manager->persist($research2);

        $research3 = new Research();
        $research3->setName('Waarom zijn bananen krom');
        $research3->setLocation('Kapetown');
        $research3->setCompany('Chiqita');
        $research3->setAvailabilityStarts(new \DateTime('yesterday 13:00:00'));
        $research3->setAvailabilityEnds(new \DateTime('tomorrow 13:00:00'));
        $research3->setStartDate(new \DateTime('02-01-2016'));
        $research3->setEndDate(new \DateTime('28-12-2016'));
        $research3->setInLanguage('English');
        $research3->setOrganizer('World Food Agency');
        $research3->setDescription('Whit a mission to create straigth bananas, we wonder why are they krom');
        $research3->setlongDescription('Most bananas are stil krom, lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus vel velit at dapibus. Mauris ut nulla sit amet erat molestie lacinia. Quisque nec sagittis urna. Vivamus ex elit, ullamcorper vitae tellus vel, egestas varius neque. Maecenas venenatis venenatis ipsum, sit amet vestibulum quam sollicitudin ac. Ut ornare, ligula ac elementum venenatis, nulla libero consequat risus, eu luctus tortor leo nec lorem. Curabitur fermentum sed neque sit amet elementum.');
        $research3->setOwner($oscar);
        $manager->persist($research3);

        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }


}
