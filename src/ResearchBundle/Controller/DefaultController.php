<?php

namespace ResearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ResearchBundle:Default:index.html.twig');
    }

    public function overviewAction($country)
    {
        //Get current researches from database doctrine and
        //pass de object to the twig template
        //Use a for loop in the template to go over the object

        return $this->render('ResearchBundle:Overview:overview.html.twig', array("country" => $country));
    }
}

