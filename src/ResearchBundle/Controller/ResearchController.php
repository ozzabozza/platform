<?php

namespace ResearchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ResearchBundle\Entity\Research;
use ResearchBundle\Form\ResearchType;

/**
 * Research controller.
 *
 */
class ResearchController extends Controller
{
    /**
     * Lists all Research entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $researches = $em->getRepository('ResearchBundle:Research')->findAll();

        return $this->render('research/index.html.twig', array(
            'researches' => $researches,
        ));
    }

    /**
     * Creates a new Research entity.
     *
     */
    public function newAction(Request $request)
    {
        $research = new Research();
        $form = $this->createForm('ResearchBundle\Form\ResearchType', $research);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getUser();   // Get current User
            $research->setOwner($user); // Set as Owner on Research entity


            $em = $this->getDoctrine()->getManager();
            $em->persist($research);
            $em->flush();

            return $this->redirectToRoute('research_show', array('id' => $research->getId()));
        }

        return $this->render('research/new.html.twig', array(
            'research' => $research,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Research entity.
     *
     */
    public function showAction(Research $research)
    {
        $deleteForm = $this->createDeleteForm($research);

        return $this->render('research/show.html.twig', array(
            'research' => $research,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Research entity.
     *
     */
    public function editAction(Request $request, Research $research)
    {
        $deleteForm = $this->createDeleteForm($research);
        $editForm = $this->createForm('ResearchBundle\Form\ResearchType', $research);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($research);
            $em->flush();

            return $this->redirectToRoute('research_edit', array('id' => $research->getId()));
        }

        return $this->render('research/edit.html.twig', array(
            'research' => $research,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Research entity.
     *
     */
    public function deleteAction(Request $request, Research $research)
    {
        $form = $this->createDeleteForm($research);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($research);
            $em->flush();
        }

        return $this->redirectToRoute('research_index');
    }

    /**
     * Creates a form to delete a Research entity.
     *
     * @param Research $research The Research entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Research $research)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('research_delete', array('id' => $research->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
