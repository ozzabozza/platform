<?php

namespace ResearchBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ResearchRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ResearchRepository extends EntityRepository
{
}
