platform
========

A Symfony project created on April 10, 2016, 8:28 pm.

1.	clone and run $ php composer.phar install
2.	create database and change app/config/parameters.yml as needed
3.	create tables run $ php app/console doctrine:schema:update --force
4.	load fixtures $ php app/console doctrine:fixtures:load
5. 	move assets to public web folder $ app/console assets:install web

Permission problems
When permission problems occur see: https://symfony.com/doc/2.8/setup/file_permissions.html
